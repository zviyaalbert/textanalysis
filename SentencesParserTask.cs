﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TextAnalysis
{
    static class SentencesParserTask
    {
        public static List<List<string>> ParseSentences(string text)
        {
            var sentencesList = new List<List<string>>();
            char[] sentenceDelimeters = { '.', '!', ';', ':', '(', ')', '?' };

            string[] sentences = text.Split(sentenceDelimeters);

            foreach (string sentence in sentences)
            {
                var punctuation = sentence.Where(f => !char.IsLetter(f) && f != '\'').ToArray();
                var words = sentence.Split(punctuation);
                List<string> wordList = new List<string>();
                foreach (var word in words)
                {
                    var s = word.ToLower().Trim();
                    if (s != String.Empty)
                    {
                        wordList.Add(s);
                    }
                }
                if (wordList.Count() != 0)
                {
                    sentencesList.Add(wordList);
                }
            }
            return sentencesList;
        }
    }
}