﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TextAnalysis
{
    static class FrequencyAnalysisTask
    {
        public static Dictionary<string, string> GetMostFrequentNextWords(List<List<string>> text)
        {
            {
                Dictionary<string, Dictionary<string, int>> pairs = new Dictionary<string, Dictionary<string, int>>();
                foreach (var sentence in text)
                {
                    for (int i = 0; i < sentence.Count - 1; i++)
                    {
                        if (pairs.ContainsKey(sentence[i]))
                        {
                            if (pairs[sentence[i]].ContainsKey(sentence[i + 1]))
                                pairs[sentence[i]][sentence[i + 1]]++;
                            else
                                pairs[sentence[i]][sentence[i + 1]] = 1;
                        }
                        else
                        {
                            pairs[sentence[i]] = new Dictionary<string, int> { { sentence[i + 1], 1 } };
                        }
                    }
                }

                Dictionary<string, string> result = new Dictionary<string, string>();

                foreach (var item in pairs)
                {
                    result.Add(item.Key, item.Value.OrderByDescending(x => x.Value).ThenBy(s => s.Key, StringComparer.Ordinal).First().Key);
                }

                //-----------------------------------------------------------------------------------------------------------------------------------

                Dictionary<string, Dictionary<string, int>> triples = new Dictionary<string, Dictionary<string, int>>();
                foreach (var sentence in text)
                {
                    for (int i = 0; i < sentence.Count - 1; i++)
                    {
                        if (triples.ContainsKey(sentence[i] + " " + sentence[i + 1]))
                        {
                            if (i + 2 < sentence.Count())
                                if (triples[sentence[i] + " " + sentence[i + 1]].ContainsKey(sentence[i + 2]))
                                {
                                    if (i + 2 < sentence.Count())
                                        triples[sentence[i] + " " + sentence[i + 1]][sentence[i + 2]]++;
                                }
                                else
                                {
                                    if (i + 2 < sentence.Count())
                                        triples[sentence[i] + " " + sentence[i + 1]][sentence[i + 2]] = 1;
                                }
                        }
                        else
                        {
                            if (i + 2 < sentence.Count())
                            {
                                triples[sentence[i] + " " + sentence[i + 1]] = new Dictionary<string, int> { { sentence[i + 2], 1 } };
                            }
                        }
                    }
                }

                foreach (var item in triples)
                {
                    result.Add(item.Key, item.Value.OrderByDescending(x => x.Value).ThenBy(s => s.Key, StringComparer.Ordinal).First().Key);
                }

                return result;
            }
        }
    }
}