﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TextAnalysis
{
    static class TextGeneratorTask
    {
        public static string ContinuePhrase(
            Dictionary<string, string> nextWords,
            string phraseBeginning,
            int wordsCount)
        {
            string curKey = phraseBeginning;
            string val = "";
            if (wordsCount > 0 && nextWords.Count > 0)
            {
                string curVal = "";
                for (int i = 0; i < wordsCount; i++)
                {
                    if (curKey.Split(' ').Count() > 2)
                    {
                        curKey = curKey.Split(' ')[curKey.Split(' ').Length - 2] + " " + curKey.Split(' ')[curKey.Split(' ').Length - 1];
                    }
                    if (nextWords.TryGetValue(curKey, out curVal))
                    {
                        val += " " + curVal;
                    }
                    else if (curKey.Split(' ').Count() == 2 && nextWords.TryGetValue(curKey.Split(' ')[curKey.Split(' ').Length - 1], out curVal))
                    {
                        val += " " + curVal;
                    }
                    else if (curKey.Split(' ').Count() == 1 && nextWords.TryGetValue(curKey, out curVal))
                    {
                        val += " " + curVal;
                    }
                    else break;
                    curKey = curKey + val;
                }
            }
            return phraseBeginning + val;
        }
    }
}