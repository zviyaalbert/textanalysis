﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnitLite;

namespace TextAnalysis
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            // Запуск автоматических тестов. Ниже список тестовых наборов, который нужно запустить.
            var testsToRun = new string[]
            {
                "TextAnalysis.SentencesParser_Tests",
                "TextAnalysis.FrequencyAnalysis_Tests",
                "TextAnalysis.TextGenerator_Tests",
            };
            new AutoRun().Execute(new[]
            {
                "--stoponerror", // Останавливать после первого же непрошедшего теста. Закомментируйте, чтобы увидеть все падающие тесты
                "--noresult",
                "--test=" + string.Join(",", testsToRun)
            });

            //Да, я тестировала на Гарри Поттере, а потом лень было менять название файла
            var text = File.ReadAllText("HarryPotterText.txt");
            var sentences = SentencesParserTask.ParseSentences(text);
            var frequency = FrequencyAnalysisTask.GetMostFrequentNextWords(sentences);
            while (true)
            {
                Console.Write("Введите первое слово (например, Аллах): ");
                var beginning = Console.ReadLine();
                if (string.IsNullOrEmpty(beginning)) return;
                var phrase = TextGeneratorTask.ContinuePhrase(frequency, beginning.ToLower(), 60);
                Console.WriteLine(phrase);
            }
        }
    }
}